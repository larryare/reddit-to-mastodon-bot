from time import sleep

import praw
from mastodon import Mastodon
from praw import models

import config

reddit = praw.Reddit(
    client_id=config.client_id,
    client_secret=config.client_secret,
    user_agent=config.user_agent,
)

# Mastodon.create_app(
#     'Combobulator',
#     api_base_url=config.mastodon_url,
#     to_file='worldnews_clientcred.secret'
# )

mastodon = Mastodon(
    client_id="worldnews_clientcred.secret", api_base_url=config.mastodon_url
)
mastodon.log_in(config.mastodon_email, config.mastodon_pass)
submission_id = ""

while True:

    try:
        submission: models.Submission = next(reddit.subreddit("worldnews").new(limit=1))
    except:
        pass
    else:
        if not submission_id == submission.id:
            submission_id = submission.id
            news = submission.title + "\n" + "🛸" + " Sauce: " + submission.url + '\n' + '☢️' + " Comments: " + submission.shortlink
            try:
                new_status = mastodon.status_post(news)
            except:
                pass

    sleep(300)
